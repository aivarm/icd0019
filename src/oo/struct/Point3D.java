package oo.struct;

public class Point3D {

    Integer x;
    Integer y;
    Integer z;

    public Point3D(Integer x, Integer y, Integer z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
}
