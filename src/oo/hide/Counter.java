package oo.hide;

public class Counter {

    private int start;
    private int step;
    private int result = 0;

    public Counter(Integer start, Integer step) {
        this.start = start;
        this.step = step;
    }

    public Integer nextValue() {
        if (result == 0) {
            result += start;
            return result;
        }
        result += step;
        return result;
    }
}
