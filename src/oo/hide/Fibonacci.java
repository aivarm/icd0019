package oo.hide;

public class Fibonacci {

    private int counter = 0;
    private int previous = 1;
    private int prevPrev = 0;

    public Integer nextValue() {
        if (counter == 0) {
            counter++;
            return 0;
        } else if (counter == 1) {
            counter++;
            return 1;
        }
        int result = previous + prevPrev;
        prevPrev = previous;
        previous = result;
        return result;
    }

    private Integer fib(int n) {
        int result;// = (n - previous) + (n - prevPrev);
        prevPrev = n - previous;
        previous = n - prevPrev;
        counter = prevPrev + previous;

        return counter;
    }

}
