package oo.hide;

public class PointSet {

    private int counter = 0;
    private Point[] pointSet;

    public PointSet(Integer initialCapacity) {
        this.pointSet = new Point[initialCapacity];
    }

    public PointSet() {
        this.pointSet = new Point[100];
    }

    public void add(Point point) {
        if (counter == pointSet.length) {
            Point[] increasedPointSet = new Point[counter * 2];
            System.arraycopy(pointSet, 0, increasedPointSet, 0, counter);
            pointSet = increasedPointSet;
        }
        if (!contains(point)) {
            pointSet[counter] = point;
            counter++;
        }
    }

    public Integer size() {
        return counter;
    }

    public boolean contains(Point p) {
        for (Point point: pointSet) {
            if (p.equals(point)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size(); i++) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(pointSet[i].toString());
        }
        return sb.toString();
    }
}
