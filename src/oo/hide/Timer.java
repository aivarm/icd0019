package oo.hide;

public class Timer {

    long start;

    public Timer() {
        this.start = System.currentTimeMillis();
    }

    public String getPassedTime() {
        double result = System.currentTimeMillis() - this.start;
        return String.valueOf(result / 1000);
    }
}
