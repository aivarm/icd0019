package exceptions.bad;

public class BadTryCatch {

    public boolean containsSingleLetters(String input) {

        if (input == null || input.length() == 0) {
            return false;
        }

        int index = 0;

        try {
            while (index < input.length()) {
                if (input.charAt(index) == input.charAt(index + 1)) {
                    return false;
                }

                index++;
            }
        } catch (Exception e) {
            return true;
        }

        return true;
    }


}
