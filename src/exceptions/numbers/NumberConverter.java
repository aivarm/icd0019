package exceptions.numbers;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Objects;
import java.util.Properties;

public class NumberConverter {

    private Properties properties = new Properties();

    public NumberConverter(String lang) {

        String filePath = String.format(
                "src/exceptions/numbers/numbers_%s.properties", lang);

        FileInputStream is = null;

        try {
            is = new FileInputStream(filePath);

            InputStreamReader reader = new InputStreamReader(
                    is, Charset.forName("UTF-8"));

            properties.load(reader);
        } catch (FileNotFoundException e) {
            throw new MissingLanguageFileException();
        } catch (IllegalArgumentException e) {
            throw new BrokenLanguageFileException();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            close(is);
        }
    }

    public String numberInWords(Integer number) {
        checkForMissingTranslations();

        if (properties.containsKey(String.valueOf(number))) {
            return properties.getProperty(String.valueOf(number));
        } else {
            int tens = number / 10;
            int ones = number - tens * 10;
            if (number < 100) {
                return generateTranslation(number, 0, tens, ones);
            } else {
                int hundreds = number / 100;
                return generateTranslation(number, hundreds, tens, ones);
            }
        }
    }

    private void checkForMissingTranslations() {
        String[] neededKeys = {"0","1","2","3","4","5","6","7","8","9","10",
                        "teen","tens-suffix","tens-after-delimiter","hundred","hundreds-before-delimiter"};

        for (String key : neededKeys) {
            if (!properties.containsKey(key) || properties.getProperty(key) == null) {
                System.out.println("key " + key + " is missing!");
                throw new MissingTranslationException();
            }
        }

    }

    private String generateTranslation(int number, int hundreds, int tens, int ones) {
        StringBuilder sb = new StringBuilder();

        if (properties.containsKey(String.valueOf(number))) {
            sb.append(properties.getProperty(String.valueOf(number)));
        } else if (ones > 0 && number <= 10) {
            sb.append(properties.getProperty(String.valueOf(ones)));
        } else if (number % 10 == 0 && number <= 90) {
            sb.append(properties.getProperty(String.valueOf(tens)));
            sb.append(properties.getProperty("tens-suffix"));
        } else if (number <= 19) {
            sb.append(properties.getProperty(String.valueOf(ones)));
            sb.append(properties.getProperty("teen"));
        } else if (number <= 99) {
            if (properties.containsKey(String.valueOf(number - ones))) {
                sb.append(properties.getProperty(String.valueOf(number - ones)));
                sb.append(properties.getProperty("tens-after-delimiter"));
                sb.append(properties.getProperty(String.valueOf(ones)));
            } else {
                sb.append(properties.getProperty(String.valueOf(tens)));
                sb.append(properties.getProperty("tens-suffix"));
                sb.append(properties.getProperty("tens-after-delimiter"));
                sb.append(properties.getProperty(String.valueOf(ones)));
            }
        } else {
                int remainder = number - hundreds * 100;
                tens = remainder / 10;

                sb.append(properties.getProperty(String.valueOf(hundreds)));
                sb.append(properties.getProperty("hundreds-before-delimiter"));
                sb.append(properties.getProperty("hundred"));

                if (tens != 0 || ones != 0) {
                    sb.append(properties.getProperty("hundreds-before-delimiter"));
                    if (Objects.equals(properties.getProperty("hundreds-before-delimiter"), "")) {
                        sb.append(properties.getProperty("tens-after-delimiter"));
                    }

                    sb.append(generateTranslation(remainder, 0, tens, ones));
                }
            }
        return sb.toString();
    }

    private static void close(FileInputStream is) {
        if (is == null) {
            return;
        }

        try {
            is.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

}
