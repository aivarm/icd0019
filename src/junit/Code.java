package junit;

public class Code {

    public static boolean isSpecial(Integer number) {
        return number % 11 == 0 || (number - 1) % 11 == 0;
    }

    public static Integer longestStreak(String input) {
        if (input.length() == 0) {
            return 0;
        } else if (input.length() == 1) {
            return 1;
        } else {
            int longestStreak = 0;
            String result = "";
            for (int i = 0; i < input.length(); i++) {
                if (i == 0) {
                    //first letter
                    result = String.valueOf(input.charAt(i));
                } else {
                    if (input.charAt(i - 1) == input.charAt(i)) {
                        //next letter if same as previous
                        result += input.charAt(i);
                    } else {
                        //reset letter
                        result = String.valueOf(input.charAt(i));
                    }
                }
                //set longest streak if new streak is bigger
                if (result.length() > longestStreak) {
                    longestStreak = result.length();
                }
            }
            return longestStreak;
        }
    }

    public static Integer[] removeDuplicates(Integer[] input) {
        Integer[] subResult = new Integer[input.length];
        int resultConter = 0;
        for (int i = 0; i < input.length; i++) {
            if (!arrayContains(subResult, input[i])) {
                subResult[i] = input[i];
                resultConter++;
            }
        }

        Integer[] result = new Integer[resultConter];
        for (int i = 0; i < result.length; i++) {
            result[i] = subResult[i];
        }

        return result;
    }

    private static boolean arrayContains(Integer[] input, Integer intToFind) {
        for (Integer integer : input) {
            if (intToFind.equals(integer)) {
                return true;
            }
        }
        return false;
    }

    public static Integer sumIgnoringDuplicates(Integer[] integers) {
        Integer[] subResult = removeDuplicates(integers);
        Integer result = 0;

        for (Integer item : subResult) {
            result += item;
        }

        return result;
    }
}
