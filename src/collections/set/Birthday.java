package collections.set;

import org.junit.Test;

import java.util.Random;


public class Birthday {

    @Test
    public void runCode() {

        Random r = new Random();

        int randomDayOfYear = r.nextInt(365);

        // pick random day in a loop
        // find how many iterations till first collision (got the same number)
        int counter = 0;
        while (r.nextInt(365) != randomDayOfYear) {
            counter++;
        }

        System.out.println("Group size must be of " + counter);

    }

}
