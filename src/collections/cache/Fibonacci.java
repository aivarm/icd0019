package collections.cache;

import java.util.HashMap;
import java.util.Map;

public class Fibonacci {

    Map<Integer, Integer> cache = new HashMap<>();

    public Integer fib(Integer n) {
//        if (n < 1) {
//            return 0;
//        }
//        if (n == 1) {
//            return 1;
//        }
//
//        return fib(n - 1) + fib(n - 2);

        if (!cache.containsKey(n)) {
            if (n < 1) {
                cache.put(0, 0);
            } else if (n == 1) {
                cache.put(1, 1);
            } else {
                cache.put(n, fib(n - 1) + fib(n - 2));
            }
        }

        return cache.get(n);
    }

}
