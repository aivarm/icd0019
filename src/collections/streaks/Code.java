package collections.streaks;

import java.util.ArrayList;
import java.util.List;

public class Code {

    public static List<List<String>> getStreakList(String input) {
        List<List<String>> result = new ArrayList<>();

        if (input.equals("")) return result;

        for (int i = 0; i < input.length(); i++) {
            String newChar = String.valueOf(input.charAt(i));
            List<String> newItem = new ArrayList<>();
            if (result.isEmpty()) {
                newItem.add(newChar);
            } else {
                boolean resultContainsItem = false;
                for (List<String> resultItem: result){
                    if (resultItem.contains(newChar)) {
                        resultItem.add(newChar);
                        resultContainsItem = true;
                        break;
                    }
                }
                if (!resultContainsItem) newItem.add(newChar);
            }

            if (!newItem.isEmpty()) result.add(newItem);
        }

        return result;
    }


}
