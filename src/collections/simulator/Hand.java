package collections.simulator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Hand {

    private List<Card> cards = new ArrayList<>();

    void addCard(Card card) {
        cards.add(card);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Card card : cards) {
            sb
                .append("Card {")
                .append(card.getSuit())
                .append(", ")
                .append(card.getValue())
                .append("}");
        }
        return sb.toString();
    }

    public boolean isOnePair() {
        if (cards.size() >= 2 && !isFullHouse() && !isTwoPairs()) {
            Map<String, List<Card>> cardMap = getCardMap(cards);

            for (List<Card> items : cardMap.values()) {
                if (items.size() == 2) return true;
            }
        }
        return false;
    }

    private Map<String, List<Card>> getCardMap(List<Card> cards) {
        Map<String, List<Card>> result = new HashMap<>();
        for (Card card : cards) {
            if (!result.containsKey(card.getValue().toString())) {
                result.put(card.getValue().toString(), new ArrayList<>(Arrays.asList(card)));
            } else {
                String cardId = card.getValue().toString();
                List<Card> currentCardList = result.get(cardId);
                currentCardList.add(card);
                result.put(cardId, currentCardList);
            }
        }
        return result;
    }

    public boolean isTwoPairs() {
        boolean hasFirstPair = false;
        boolean hasSecondPair = false;
        if (cards.size() >= 4 && !isFullHouse()) {
            Map<String, List<Card>> cardMap = getCardMap(cards);

            for (List<Card> items : cardMap.values()) {
                if (items.size() == 2) {
                    if (!hasFirstPair) {
                        hasFirstPair = true;
                    } else {
                        hasSecondPair = true;
                    }
                }
            }
        } else {
            return false;
        }
        return hasFirstPair && hasSecondPair;
    }

    public boolean isTrips() {
        if (cards.size() >= 3 && !isFullHouse()) {
            Map<String, List<Card>> cardMap = getCardMap(cards);

            for (List<Card> items : cardMap.values()) {
                if (items.size() == 3) return true;
            }
        }
        return false;
    }

    public boolean isFullHouse() {
        boolean hasOnePair = false;
        boolean hasTrips = false;
        if (cards.size() == 5) {
            Map<String, List<Card>> cardMap = getCardMap(cards);

            for (List<Card> items : cardMap.values()) {
                if (items.size() == 2 && !hasOnePair) {
                    hasOnePair = true;
                } else if (items.size() == 3 && !hasTrips) {
                    hasTrips = true;
                }
            }
        } else {
            return false;
        }
        return hasOnePair && hasTrips;
    }

}
