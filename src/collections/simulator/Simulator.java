package collections.simulator;

import java.util.HashMap;
import java.util.Map;

public class Simulator {
    public final Double TOTAL_FREQUENCY = nCk(52, 5);

    public Map<HandType, Double> calculateProbabilities() {
        Map<HandType, Double> result = new HashMap<>();

        //Pair freq
        Double pairFreq = nCk(13, 1) * nCk(4, 2) * nCk(12, 3) * Math.pow(nCk(4, 1), 3);
        result.put(HandType.PAIR, findProb(pairFreq));

        //Two pair freq
        Double twoPairFreq = nCk(13, 2) * Math.pow(nCk(4, 2), 2) * nCk(11, 1) * nCk(4, 1);
        result.put(HandType.TWO_PAIRS, findProb(twoPairFreq));

        //Three of a kind freq
        Double threeOfKindFreq = nCk(13, 1) * nCk(4, 3) * nCk(12, 2) * Math.pow(nCk(4, 1), 2);
        result.put(HandType.THREE_OF_A_KIND, findProb(threeOfKindFreq));

        //Full house freq
        Double fullHouseFreq = nCk(13, 1) * nCk(4, 3) * nCk(12, 1) * nCk(4, 2);
        result.put(HandType.FULL_HOUSE, findProb(fullHouseFreq));

        return result;
    }

    private Double findProb(Double frequency) {
        //Calculate x percent
        return (frequency * 100) / TOTAL_FREQUENCY;
    }

    private Double nCk(int n, int k) {
        return factorial(n) / ( factorial(k) * factorial(n - k));
    }

    private Double factorial(int n) {
        double result = 1.0;
        for (int i = 2; i <= n; i++) {
            result = result * i;
        }
        return result;
    }

}
