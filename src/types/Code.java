package types;

import java.util.Objects;

public class Code {

    public static void main(String[] args) {

        Integer[] numbers = new Integer[] {1, 3, -2, 9};

        System.out.println(sum(numbers)); // 11
        System.out.println(average(numbers));
        System.out.println(minimumElement(numbers));
        System.out.println(asString(numbers));
        System.out.println(squareDigits("2"));
        System.out.println(squareDigits("a2b"));
        System.out.println(squareDigits("a22b"));
        System.out.println(squareDigits("a9b2"));
    }

    public static Integer sum(Integer[] numbers) {
        Integer result = 0;

        for (Integer number : numbers) {
            result += number;
        }
        return result;
    }

    public static Double average(Integer[] numbers) {
        Double result = Double.valueOf(sum(numbers));

        return result / numbers.length;
    }

    public static Integer minimumElement(Integer[] integers) {
        Integer result = null;

        if (integers.length == 0) {
            return null;
        }

        for (Integer number : integers) {
            if (result == null || number < result) {
                result = number;
            }
        }

        return result;
    }

    public static String asString(Integer[] elements) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < elements.length; i++) {
            result.append(elements[i].toString());

            if (i + 1 < elements.length) {
                result.append(", ");
            }
        }
        return result.toString();
    }

    public static String squareDigits(String s) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            if (Character.isDigit(c)) {
                Integer integer = Integer.parseInt(Character.toString(c));
                result.append(integer * integer);
            } else {
                result.append(c);
            }
        }
        return result.toString();
    }


}
