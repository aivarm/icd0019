package inheritance.calc;

public class TaxFreePayCalculator extends PayCalculator {

    private static final Double TAX_FREE_RATE = 0.0;

    @Override
    public Double getWeeklyPayAfterTaxes(Integer hoursWorked) {
        return super.getWeeklyPayAfterTaxes(hoursWorked);
    }

    @Override
    public Double getTaxRate() {
        return TAX_FREE_RATE;
    }

}
