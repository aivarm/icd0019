package inheritance.stack;

import java.util.Stack;

public class ExtendedStack extends Stack<Integer> {

    ExtendedStack() {
        System.out.println("Initializing ExtendedStack");
    }

    @Override
    public Integer push(Integer var1) {
        System.out.println("Initializing ExtendedStack.push()");
        return super.push(var1);
    }

    @Override
    public synchronized Integer pop() {
        System.out.println("Initializing ExtendedStack.pop()");
        return super.pop();
    }

    void pushAll(Integer... numbers) {
        if (numbers.length > 0) {
            for (Integer number : numbers) {
                push(number);
            }
        }
    }
}
