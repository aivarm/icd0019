package inheritance.sender;

import java.time.LocalTime;

class EmailLetterSender extends AbstractLetterSender {

    EmailLetterSender(LocalTime currentTime) {
        super.letterSender(currentTime);
    }

    void sendLetter() {
        String greeting = "Good " + getTimeOfDayString();
        String contents = "Dead customer, ...";

        // simulate sending the email

        System.out.println("Sending email ...");
        System.out.println("Title: " + greeting);
        System.out.println("Text: " + contents);
    }

}
