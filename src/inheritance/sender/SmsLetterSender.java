package inheritance.sender;

import java.time.LocalTime;

class SmsLetterSender extends AbstractLetterSender {

    SmsLetterSender(LocalTime currentTime) {
        super.letterSender(currentTime);
    }

    void sendLetter() {
        String greeting = "Good " + getTimeOfDayString();
        String contents = "Dead customer, ...";

        // simulate sending the sms

        System.out.println("Sending sms ...");
        System.out.println("Title: " + greeting);
        System.out.println("Text: " + contents);
    }

}
