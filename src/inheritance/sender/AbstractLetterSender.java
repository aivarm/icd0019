package inheritance.sender;

import java.time.LocalTime;

class AbstractLetterSender {

    private LocalTime currentTime;

    void letterSender(LocalTime currentTime) {
        this.currentTime = currentTime;
    }

    String getTimeOfDayString() {

        int hour = currentTime.getHour();

        if (hour > 5 && hour <= 11) {
            return "morning";
        } else if (hour > 11 && hour <= 17) {
            return "afternoon";
        } else if (hour > 17 && hour <= 20) {
            return "evening";
        } else {
            return "night";
        }
    }
}
