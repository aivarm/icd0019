package inheritance.analyser;

public class DifferentiatedTaxSalesAnalyser extends AbstractSalesAnalyzer {

    private static final Double REGULAR_TAX = 0.2;
    private static final Double REDUCED_TAX = 0.1;

    DifferentiatedTaxSalesAnalyser(SalesRecord[] records) {
        super(records);
    }

    public String getIdOfMostPopularItem() {
        throw new RuntimeException("not implemented yet");
    }

    public String getIdOfItemWithLargestTotalSales() {
        throw new RuntimeException("not implemented yet");
    }

    @Override
    public Double getTax(boolean isReduced) {
        if (isReduced) {
            return REDUCED_TAX;
        }
        return REGULAR_TAX;
    }

}
