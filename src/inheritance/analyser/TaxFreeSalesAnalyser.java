package inheritance.analyser;

public class TaxFreeSalesAnalyser extends AbstractSalesAnalyzer {

    private static final Double FREE_TAX = 0.0;

    TaxFreeSalesAnalyser(SalesRecord[] records) {
        super(records);
    }

    public String getIdOfMostPopularItem() {
        throw new RuntimeException("not implemented yet");
    }

    public String getIdOfItemWithLargestTotalSales() {
        throw new RuntimeException("not implemented yet");
    }

    @Override
    public Double getTax(boolean isReduced) {
        return FREE_TAX;
    }
}
