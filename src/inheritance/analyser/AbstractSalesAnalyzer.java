package inheritance.analyser;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Map;

abstract class AbstractSalesAnalyzer {

    private SalesRecord[] records;

    private static final Double DEFAULT_TAX_RATE = 0.0;

    AbstractSalesAnalyzer(SalesRecord[] records) {
        this.records = records;
    }

    Double getTotalSales() {
        double totalSales = 0.0;
        for (SalesRecord record : records) {
            totalSales += calculateSale(record);
        }

        return totalSales;
    }

    Double getTotalSalesByProductId(String id) {
        if (id != null) {
            double totalSalesByProductId = 0.0;
            for (SalesRecord record: this.records) {
                if (id.equals(record.getProductId())) {
                    totalSalesByProductId += calculateSale(record);
                }
            }
            return totalSalesByProductId;
        }
        return 0.0;
    }

    public String getIdOfMostPopularItem() {
        LinkedHashMap<String, Integer> recordsByItemCount = new LinkedHashMap<>();

        // Counting for total items
        for (SalesRecord record : this.records) {
            if (recordsByItemCount.containsKey(record.getProductId())) {
                Integer currentCount = recordsByItemCount.get(record.getProductId());
                recordsByItemCount.put(record.getProductId(), currentCount + record.getItemsSold());
            } else {
                recordsByItemCount.put(record.getProductId(), record.getItemsSold());
            }
        }

        return getKeyByMaxValue(recordsByItemCount);
    }

    public String getIdOfItemWithLargestTotalSales() {
        LinkedHashMap<String, Integer> recordsByTotal = new LinkedHashMap<>();

        // Counting for total sales
        for (SalesRecord record : this.records) {
            if (recordsByTotal.containsKey(record.getProductId())) {
                Integer currentCount = recordsByTotal.get(record.getProductId());
                recordsByTotal.put(record.getProductId(), currentCount + 1);
            } else {
                recordsByTotal.put(record.getProductId(), 1);
            }
        }

        return getKeyByMaxValue(recordsByTotal);
    }

    private String getKeyByMaxValue(LinkedHashMap<String, Integer> records) {
        Integer maxCount = Collections.max(records.values());
        // Find entry for maxCount
        for (Map.Entry<String, Integer> entry : records.entrySet()) {
           if (entry.getValue().equals(maxCount)) {
               return entry.getKey();
           }
        }
        return "";
    }

    public Double getTax(boolean isReduced) {
        return DEFAULT_TAX_RATE;
    }

    private Double calculateSale(SalesRecord record) {
        return record.getItemsSold() * (record.getProductPrice() / (1 + getTax(record.hasReducedRate())));
    }

}
