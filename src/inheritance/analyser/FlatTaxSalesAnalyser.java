package inheritance.analyser;

public class FlatTaxSalesAnalyser extends AbstractSalesAnalyzer {

    private static final Double FLAT_TAX = 0.2;

    FlatTaxSalesAnalyser(SalesRecord[] records) {
        super(records);
    }

    @Override
    public Double getTax(boolean isReduced) {
        return FLAT_TAX;
    }
}
